package main

import (
	"log"
	"sync"
)

// InMemoryStorage implements the StorageInterface for in-memory transaction storage.
type InMemoryStorage struct {
	transactionStorage map[string][]Transaction
	subscribedAddresses map[string]bool
	mutex              sync.RWMutex
}

// NewInMemoryStorage initializes and returns a new InMemoryStorage instance.
func NewInMemoryStorage() *InMemoryStorage {
	return &InMemoryStorage{
		transactionStorage: make(map[string][]Transaction),
		subscribedAddresses: make(map[string]bool),
	}
}

// AddTransaction adds a transaction to the storage.
func (s *InMemoryStorage) AddTransaction(address string, transaction Transaction) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if _, exists := s.transactionStorage[address]; !exists {
		s.transactionStorage[address] = []Transaction{} // Initialize the slice if it does not exist
	}

	s.transactionStorage[address] = append(s.transactionStorage[address], transaction)
	log.Printf("Transaction added for address %s: %+v", address, transaction)
}

// GetTransactionsForAddress retrieves transactions for a specific address.
func (s *InMemoryStorage) GetTransactionsForAddress(address string) []Transaction {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	if transactions, exists := s.transactionStorage[address]; exists {
		return transactions
	}
	return []Transaction{} // Return an empty slice if no transactions are found
}

// IsSubscribed checks if an address is subscribed.
func (s *InMemoryStorage) IsSubscribed(address string) bool {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	_, exists := s.subscribedAddresses[address]
	return exists
}

// Subscribe adds an address to the list of subscribed addresses if it's not already subscribed.
func (s *InMemoryStorage) Subscribe(address string) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if _, exists := s.subscribedAddresses[address]; !exists {
		s.subscribedAddresses[address] = true
		log.Printf("Address %s subscribed successfully", address)
		return true
	}
	log.Printf("Address %s is already subscribed", address)
	return false
}

// Variable to hold the singleton instance of InMemoryStorage
var storageInstance *InMemoryStorage

// GetStorageInstance returns the singleton instance of InMemoryStorage, creating it if necessary.
func GetStorageInstance() *InMemoryStorage {
	if storageInstance == nil {
		storageInstance = NewInMemoryStorage()
	}
	return storageInstance
}