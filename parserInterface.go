package main

// Parser interface defines the operations that the Ethereum transaction parser must support.
type Parser interface {
	// GetCurrentBlock returns the last parsed block number as an integer.
	GetCurrentBlock() int

	// Subscribe adds an address to the observer list and returns a boolean indicating success.
	Subscribe(address string) bool

	// IsSubscribed checks if an address is already subscribed.
	IsSubscribed(address string) bool

	// GetTransactions lists inbound or outbound transactions for a specified address.
	GetTransactions(address string) []Transaction

	// UpdateCurrentBlockNumber updates the current block number.
	UpdateCurrentBlockNumber(blockNumber string)

	// AddTransaction adds a transaction for a specified address.
	AddTransaction(address string, transaction Transaction)
}