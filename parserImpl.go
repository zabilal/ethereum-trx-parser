package main

import (
	"log"
	"strconv"
	"sync"
)

// parserImpl implements the Parser interface.
type parserImpl struct {
	storage            StorageInterface
	currentBlockMutex  sync.RWMutex
	currentBlockNumber string // This will store the current block number as a string
}

// NewParserImpl creates a new instance of parserImpl.
func NewParserImpl(storage StorageInterface) *parserImpl {
	return &parserImpl{
		storage: storage,
	}
}

// GetCurrentBlock returns the last parsed block number as an integer.
func (p *parserImpl) GetCurrentBlock() int {
	p.currentBlockMutex.RLock()
	defer p.currentBlockMutex.RUnlock()

	blockNumber, err := strconv.ParseInt(p.currentBlockNumber, 0, 64)
	if err != nil {
		log.Printf("Error parsing current block number: %v", err)
		return -1
	}
	return int(blockNumber)
}

// Subscribe adds an address to the observer and returns a boolean indicating success.
func (p *parserImpl) Subscribe(address string) bool {
	success := p.storage.Subscribe(address)
	if success {
		log.Printf("Address %s successfully subscribed.", address)
	} else {
		log.Printf("Failed to subscribe address %s. It might already be subscribed.", address)
	}
	return success
}

// IsSubscribed checks if an address is subscribed.
func (p *parserImpl) IsSubscribed(address string) bool {
	return p.storage.IsSubscribed(address)
}

// GetTransactions lists inbound or outbound transactions for a specified address.
func (p *parserImpl) GetTransactions(address string) []Transaction {
	transactions := p.storage.GetTransactionsForAddress(address)
	log.Printf("Retrieved %d transactions for address %s", len(transactions), address)
	return transactions
}

// UpdateCurrentBlockNumber updates the current block number in a thread-safe manner.
func (p *parserImpl) UpdateCurrentBlockNumber(blockNumber string) {
	p.currentBlockMutex.Lock()
	defer p.currentBlockMutex.Unlock()

	p.currentBlockNumber = blockNumber
	log.Printf("Current block number updated to %s", blockNumber)
}

// AddTransaction adds a transaction for a specified address.
func (p *parserImpl) AddTransaction(address string, transaction Transaction) {
	if p.IsSubscribed(address) {
		p.storage.AddTransaction(address, transaction)
		log.Printf("Transaction added for address %s: %+v", address, transaction)
	} else {
		log.Printf("Attempted to add transaction for unsubscribed address %s", address)
	}
}

// GetStorage returns the storage interface used by the parser.
func (p *parserImpl) GetStorage() StorageInterface {
	return p.storage
}