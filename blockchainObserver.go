package main

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

// Function to fetch the current block number and transactions
func getCurrentBlockNumber() {
	body := []byte(`{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":1}`)
	resp, err := http.Post("https://cloudflare-eth.com", "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Fatalf("Failed to request current block number: %v", err)
		return
	}
	defer resp.Body.Close()
	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Failed to read response body: %v", err)
		return
	}

	var result map[string]interface{}
	if err := json.Unmarshal(responseBody, &result); err != nil {
		log.Fatalf("Failed to unmarshal response body: %v", err)
		return
	}

	if blockNumber, ok := result["result"]; ok {
		log.Printf("Current Block: %s", blockNumber)

		// Update the current block number in the parser instance
		parser.UpdateCurrentBlockNumber(blockNumber.(string))

		// Fetch and filter transactions from the current block
		fetchAndFilterTransactions(blockNumber.(string))
	} else {
		log.Fatalf("Failed to get current block number: result not found in response")
	}
}

// Fetches transactions from a block and filters them based on subscribed addresses
func fetchAndFilterTransactions(blockNumber string) {
	body := []byte(`{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["` + blockNumber + `", true],"id":1}`)
	resp, err := http.Post("https://cloudflare-eth.com", "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Fatalf("Failed to fetch transactions for block %s: %v", blockNumber, err)
		return
	}
	defer resp.Body.Close()
	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Failed to read response body: %v", err)
		return
	}

	var blockResult struct {
		Result struct {
			Transactions []Transaction `json:"transactions"`
		} `json:"result"`
	}

	if err := json.Unmarshal(responseBody, &blockResult); err != nil {
		log.Fatalf("Failed to unmarshal response body: %v", err)
		return
	}

	for _, tx := range blockResult.Result.Transactions {
		currentTime := time.Now()
		// Create a new transaction instance with Timestamp for storage
		newTx := Transaction{
			Hash:      tx.Hash,
			From:      tx.From,
			To:        tx.To,
			Value:     tx.Value,
			Timestamp: currentTime,
		}

		// Check if the transaction's from or to address is subscribed
		if parser.IsSubscribed(tx.From) {
			log.Printf("Transaction from subscribed address: %+v", newTx)
			parser.AddTransaction(tx.From, newTx) // Store the new transaction for the 'from' address
		}
		if parser.IsSubscribed(tx.To) {
			log.Printf("Transaction to subscribed address: %+v", newTx)
			parser.AddTransaction(tx.To, newTx) // Store the new transaction for the 'to' address
		}
	}
}