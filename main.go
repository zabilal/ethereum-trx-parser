package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"time"
)

// Initialize the parser instance
var parser Parser

func init() {
	parser = NewParserImpl(GetStorageInstance())
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome to the Ethereum Transaction Parser!")
	})

	http.HandleFunc("/subscribe", subscribeHandler)
	http.HandleFunc("/transactions", transactionsHandler)
	http.HandleFunc("/currentBlock", currentBlockHandler)

	startBlockchainObserver()

	fmt.Println("Server is listening on port 8080...")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("Error starting server: %s\n", err)
	}
}

// subscribeHandler handles the subscription requests
func subscribeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	var request struct {
		Address string `json:"address"`
	}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		log.Printf("Error parsing JSON request body: %v", err)
		http.Error(w, "Error parsing JSON request body", http.StatusBadRequest)
		return
	}

	if !isValidAddress(request.Address) {
		http.Error(w, "Invalid Ethereum address", http.StatusBadRequest)
		return
	}

	if !parser.Subscribe(request.Address) {
		http.Error(w, "Failed to subscribe address", http.StatusInternalServerError)
		return
	}

	log.Printf("Address %s subscribed successfully", request.Address)

	response := struct {
		Message string `json:"message"`
	}{
		Message: "Address subscribed successfully",
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Printf("Error encoding response: %v", err)
		http.Error(w, "Error encoding response", http.StatusInternalServerError)
	}
}

// isValidAddress checks if the Ethereum address is valid
func isValidAddress(address string) bool {
	// Simple regex to check if an Ethereum address is valid (must start with 0x followed by 40 hex characters)
	re := regexp.MustCompile(`^0x[a-fA-F0-9]{40}$`)
	return re.MatchString(address)
}

// transactionsHandler handles the transaction queries
func transactionsHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	query := r.URL.Query()
	address := query.Get("address")

	if !isValidAddress(address) {
		http.Error(w, "Invalid Ethereum address", http.StatusBadRequest)
		return
	}

	if !parser.IsSubscribed(address) {
		http.Error(w, "Address not found in subscription list", http.StatusNotFound)
		return
	}

	transactions := parser.GetTransactions(address)

	if len(transactions) == 0 {
		log.Printf("No transactions found for address: %s", address)
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(transactions); err != nil {
		log.Printf("Error encoding response for transactions query: %v", err)
		http.Error(w, "Error encoding response", http.StatusInternalServerError)
	}
}

func currentBlockHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	blockNumber := parser.GetCurrentBlock()
	if blockNumber == -1 {
		log.Printf("Error retrieving current block number")
		http.Error(w, "Error retrieving current block number", http.StatusInternalServerError)
		return
	}

	response := struct {
		CurrentBlock int `json:"currentBlock"`
	}{
		CurrentBlock: blockNumber,
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Printf("Error encoding response for current block query: %v", err)
		http.Error(w, "Error encoding response", http.StatusInternalServerError)
	}
}

func startBlockchainObserver() {
	ticker := time.NewTicker(10 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				log.Println("Starting blockchain observer...")
				// Implementation of getCurrentBlockNumber removed, assuming integration with blockchainObserver.go logic
				getCurrentBlockNumber()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}