package main

// StorageInterface defines the methods for managing transactions and subscriptions.
type StorageInterface interface {
	AddTransaction(address string, transaction Transaction)
	GetTransactionsForAddress(address string) []Transaction
	IsSubscribed(address string) bool
	Subscribe(address string) bool
}