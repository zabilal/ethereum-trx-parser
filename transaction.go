package main

import "time"

// Transaction represents a transaction involving subscribed Ethereum addresses.
type Transaction struct {
    Hash      string    `json:"hash"`
    From      string    `json:"from"`
    To        string    `json:"to"`
    Value     string    `json:"value"`
    Timestamp time.Time `json:"timestamp"`
}