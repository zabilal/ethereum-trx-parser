# Ethereum TRX Parser

The Ethereum TRX Parser is a cutting-edge tool designed to monitor and query Ethereum blockchain transactions in real-time for subscribed addresses. This project is especially beneficial for developers and companies building applications that need to track transactions efficiently, such as crypto wallets and notification services.

## Overview

Developed in Go, the Ethereum TRX Parser leverages the latest language features, ensuring high performance and reliability. It operates by directly interfacing with the Ethereum blockchain via JSON RPC calls, prioritizing efficiency and reducing external dependencies. The architecture consists of a REST API for user interactions, a blockchain observer to fetch new transaction data, a subscription manager for address monitoring, and an in-memory storage solution for quick data retrieval.

## Features

- **Real-time Blockchain Monitoring:** Instantly tracks transactions involving subscribed Ethereum addresses.
- **Address Subscription:** Users can easily subscribe to Ethereum addresses to start monitoring transactions.
- **Transaction Querying:** Offers the capability to query inbound and outbound transactions for subscribed addresses.
- **REST API Interface:** Provides a simple and efficient way for users to manage subscriptions and perform transaction queries.

## Getting started

### Requirements

- Go 1.21.5 or later
- Internet connection for Ethereum blockchain interactions

### Quickstart

1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Execute `go build` to compile the application.
4. Run the application with `./ethereum-trx-parser`.
5. Utilize the REST API endpoints to subscribe to addresses and query their transactions.

### License

Copyright (c) 2024.